const productList = [
  {
    name: "macbook pro",
    age: 23,
    size: "15.6",
    system: "mac OS",
  },
  {
    name: "macbook air",
    age: 24,
    size: "13",
    system: "mac OS",
  },
  {
    name: "iphone",
    age: 10,
    system: "ios",
  },
];
const list = [1, 2, 3, 4, 5, 1, 2, 3, 3, 3, 4, 5, 6, 77];
function getMaxCountItem(list) {
  const res = list.reduce((acc, item) => {
    const value = acc.has(item) ? acc.get(item) : 0;
    acc.set(item, value + 1);
    return acc;
  }, new Map());
  const max = Math.max(...res.values());
  for (let item of res) {
    const [key, value] = item;
    if (value === max) return { [key]: value };
  }
}

const num = 23553330099.35345;
function thounsandChar(num, decimal) {
  const [int, float] = num?.toString()?.split(".");
  console.log(int, float);
  const resInt = addUnderline(int);
  let resFloat = float.slice(0, decimal);
  return `${resInt}.${resFloat}`
}

const result = thounsandChar(num, 4);

function addUnderline(str, isReverse = true) {
  const list = isReverse ? str?.split("")?.reverse() : str?.split("");
  const result = list?.reduce(
    (acc, item, idx, arr) =>
      acc?.concat((idx + 1) % 3 === 0 && arr[idx + 1] ? [item, ","] : item),
    []
  );
  return isReverse ? result?.reverse()?.join("") : result?.join("");
}
// getMaxCountItem(list)
console.log(result);

function flat(num) {
    if(typeof num !== 'number') {
        throw new TypeError(`${num} is not a number`)
    }
    try {
        let [init, float] = num?.toString()?.split('.')
        init = init?.split('')?.reverse()
        const newInit = init.reduce((acc, item, idx) => acc.concat(((idx + 1) === init?.length || (idx+1)%3) ? item : [item, '_']), [])
        init = newInit.reverse().join('')
        return `${init}.${float}`
    } catch (error) {
        
    }
}
// const res = flat(num)
// console.log(res, 'res');



function myFlat(dpth) {
    if(dpth === undefined) {
        dpth = 1
    }
    const result = []
    for(let item of this) {
        if(Array.isArray(item) && dpth > 0) {
            result.push(...item.myFlat(dpth - 1))
            break;
        } 
        result.push(item)
    }
    return result
}
function myFlat1(dpth = 1) {
    return this.reduce((acc, item) => acc.concat((Array.isArray(item) && dpth > 0) ? item.myFlat1(dpth - 1) : [item] ), [])
}
Array.prototype.myFlat = myFlat
Array.prototype.myFlat1 = myFlat1

const testData = [1, [2, [3, [4, [5]]]]]
const params = 2
const native = testData.flat(params)
const custom = testData.myFlat(params)
const custom1= testData.myFlat1(params)
console.log(native, 'native', custom, custom1)